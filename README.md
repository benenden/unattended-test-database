# Unattended Test (Database) #

This test is split into 2 tasks, first you will script small schema and then second you will implement a PL/SQL package procedure.

If you don't have access to an [Oracle database](http://www.oracle.com/technetwork/database/database-technologies/express-edition/downloads/index.html) to do this development on then please mention this in your submission. Ultimately we are looking for your development style and thought processes and not for 100% syntactically correct code.

## Task 1 ##

Your first task is to write a SQL script to create the tables (including any necessary constraints and ancillary objects) to support the entities below.

### Product ###
A Product has a unique name and a cost (stored in pence), both of which are mandatory.

### Member ###
A Member has a unique membership number and also a forename and surname, both of which are mandatory.

### MemberProduct ###
A Member can have multiple Products. Each MemberProduct has a mandatory date when the next payment is due and defaults to the current date when created.

### Payment ###
Each MemberProduct can have multiple Payments, these Payments have a payment date and an amount, both are mandatory.

## Task 2 ##

Your second task is to write a PL/SQL package procedure that does the following:

1. Select all the member products that are due a payment
2. For each record selected insert a row in the Payment table
3. For each record selected update the next payment date by 1 year

Your solution should implement a logging system where the log messages persist regardless of the procedure outcome and transaction state.

## Submission ##

Your solution should include a README file to cover:

* Any assumptions you made
* How you would test your code

The output from the 2 tasks can be submitted in one of two ways:

1. Commit your solution to a public git repository (github, bitbucket etc) and email the URL
2. Zip the project and email it
